# Fuzzing projects

FUZZ TESTING (fuzzing) is a software testing technique that inputs invalid or random data called FUZZ into the software system to discover coding errors and security loopholes. Data is inputted using automated or semi-automated testing techniques.